library(ggplot2)
library(dplyr)
library(rcartocolor)
library(readr)

# df <- read_csv("Desktop/Graficas_Destimates_long.csv")
# 
# df <- read_csv("GitLab/species-diversity-plots/data/Graficas_Destimates_long.csv")

df <- read_csv("GitLab/species-diversity-plots/data/Graficas_Destimates_ci_mol.csv")

df$sample <- paste0("20",substr(df$x,4,5),"-",substr(df$x,1,3))


prep <- carto_pal(7, "Sunset")
pal <- prep[c(2, 3, 6, 7)]

unique(df$Guilds)
orden <- c("Arthropods", "Predators", "Herbivores", "Pollinators", "Detritivores")

plot1 <- df %>%
  ggplot(aes(x=factor(Guilds, level=orden), y = value, colour = sample)) +
  geom_pointrange(aes(x=factor(Guilds, level=orden), y=value, ymin=lower, ymax=upper, 
                      group=sample, color=sample), 
                  position = position_dodge(width = 0.4), size=0.1) +
  scale_color_manual(values = pal) +
  scale_y_continuous(trans='log2') 


p <- plot1 + facet_grid(rows = vars(variable),
                   scales = "free") +
  labs(x='Guilds', y='EstimateD (Effective number of species)', color = "Year and treatment") 
                   
p

